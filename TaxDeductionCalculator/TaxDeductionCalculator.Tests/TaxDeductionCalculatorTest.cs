using System;
using Xunit;

namespace TaxDeductionCalculator.Tests
{
    public class TaxDeductionCalculatorTest
    {
        [Fact]
        public void GetDeductibleAmount_Calculation()
        {
            var deductionCalculator = new DeductionCalculator();
            var amount = 1000m;
            var taxRatePercentage = 20m;
            var deductibleAmount = deductionCalculator.GetDeductibleAmount(amount, taxRatePercentage, 1);
            Assert.Equal(2.0240480961923847695390781300m, deductibleAmount);
        }

        [Fact]
        public void GetDeductibleAmount_CalculationWithPrecision()
        {
            var deductionCalculator = new DeductionCalculator();
            var amount = 1000m;
            var taxRatePercentage = 20m;
            var precision = 2;
            var deductibleAmount = deductionCalculator.GetDeductibleAmountWithPrecision(amount, taxRatePercentage, precision: precision);
            Assert.Equal(2.00m, deductibleAmount);
        }

        [Theory]
        [InlineData(-1000, 20, 2)]
        [InlineData(1000, -20, 2)]
        [InlineData(1000, 20, -1)]
        public void GetDeductibleAmount_NegativeAmount(decimal amount, decimal taxRatePercentage, int precision)
        {
            var deductionCalculator = new DeductionCalculator();
            Assert.Throws<ArgumentOutOfRangeException>(() => deductionCalculator.GetDeductibleAmountWithPrecision(amount, taxRatePercentage, precision: precision));
        }
    }
}
