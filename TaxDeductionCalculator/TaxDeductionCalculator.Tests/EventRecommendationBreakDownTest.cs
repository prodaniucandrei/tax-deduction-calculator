﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxDeductionCalculator.Recommendation;
using Xunit;

namespace TaxDeductionCalculator.Tests
{
    public class EventRecommendationBreakDownTest
    {
        [Theory]
        [InlineData(200000, 300000, 100000, 50000, 200000, 0, 0)]
        [InlineData(400000, 300000, 100000, 50000, 300000, 100000, 0)]
        [InlineData(500000, 300000, 100000, 50000, 300000, 100000, 100000)]
        public void EventRecommendationBreakDownTest_Amount3(
            decimal availableAmount,
            decimal sportsDonationLimit,
            decimal politicalDonationLimit,
            decimal otherDonationLimit,
            decimal sportsRecommendedDonation,
            decimal politicalRecommendedDonation,
            decimal otherRecommendedDonation)
        {
            var recCalc = new RecommendationBreakDown();
            var events = new List<EventInfo> {
                new EventInfo (EventType.Sports, 5, sportsDonationLimit ),
                new EventInfo (EventType.Political, 3, politicalDonationLimit ),
                new EventInfo (EventType.Other, 0, otherDonationLimit )
            };

            var result = recCalc.BreakDownAmount(availableAmount, events);
            var expected = new List<EventRecommendationBreakDown> {
                new EventRecommendationBreakDown(EventType.Sports, sportsRecommendedDonation),
                new EventRecommendationBreakDown(EventType.Political, politicalRecommendedDonation),
                new EventRecommendationBreakDown(EventType.Other, otherRecommendedDonation)
            };

            Assert.True(
                result.All(r =>
                expected.Any(e => e.EventType == r.EventType && e.Amount == r.Amount)));
        }
    }
}
