﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaxDeductionCalculator.Recommendation
{
    public class RecommendationBreakDown : IRecommendation
    {
        public IList<EventRecommendationBreakDown> BreakDownAmount(decimal amount, IEnumerable<EventInfo> events)
        {
            var response = new List<EventRecommendationBreakDown>();

            foreach (var @event in events.OrderByDescending(e => e.Supplement))
            {
                if (LeftAmountAvailable(amount, @event.DonationLimit ) 
                    && !IsLastEvent(response.Count,events.Count()))
                {
                    response.Add(new EventRecommendationBreakDown(@event.EventType, @event.DonationLimit));
                    amount -= @event.DonationLimit;
                }
                else
                {
                    response.Add(new EventRecommendationBreakDown(@event.EventType, amount));
                    amount = 0;
                }
            }
            return response;
        }

        private bool LeftAmountAvailable(decimal amount, decimal amountLimit)
        {
            return amount - amountLimit > 0;
        }

        private bool IsLastEvent(int currentEvent, int lastEvent)
        {
            return currentEvent == lastEvent-1;
        }
    }
}
