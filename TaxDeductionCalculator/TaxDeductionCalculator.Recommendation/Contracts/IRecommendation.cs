﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Recommendation
{
    public interface IRecommendation
    {
        IList<EventRecommendationBreakDown> BreakDownAmount(decimal amount, IEnumerable<EventInfo> events);
        
    }
}
