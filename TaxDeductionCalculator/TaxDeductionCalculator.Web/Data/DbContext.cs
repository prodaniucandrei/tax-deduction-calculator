﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Web.Models;

namespace TaxDeductionCalculator.Web.Data
{
    public class DbContext: IdentityDbContext<AppUser>
    {
        public DbContext(DbContextOptions<DbContext> options)
            :base(options)
        {
        }
    }
}
