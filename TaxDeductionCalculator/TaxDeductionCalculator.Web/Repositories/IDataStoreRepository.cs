﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Repositories
{
    public interface IDataStoreRepository
    {
        Task<decimal> GetCurrentTaxRate();
        Task<bool> UpdateCurrentTaxRate(decimal newTaxRate);
        Task<IList<EventInfo>> GetEventsWithInfo();
        Task<decimal> GetSuplementForDeduction(EventType eventType);
        Task<bool> UpdateEventSuplement(EventType eventType, decimal suplement);
    }
}