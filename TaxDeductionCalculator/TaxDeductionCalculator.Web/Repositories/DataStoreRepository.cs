﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Web.Services;

namespace TaxDeductionCalculator.Web.Repositories
{
    public class DataStoreRepository : IDataStoreRepository
    {
        private readonly IDataStoreService _dataStoreService;

        public DataStoreRepository(IDataStoreService dataStoreService)
        {
            _dataStoreService = dataStoreService;
        }

        public async Task<decimal> GetCurrentTaxRate()
        {
            return await _dataStoreService.GetCurrentTaxRate();
        }

        public async Task<bool> UpdateCurrentTaxRate(decimal newTaxRate)
        {
            return await _dataStoreService.UpdateCurrentTaxRate(newTaxRate);
        }

        public async Task<IList<EventInfo>> GetEventsWithInfo()
        {
            return await _dataStoreService.GetEventsWithInfo();
        }

        public Task<decimal> GetSuplementForDeduction(EventType eventType)
        {
            return _dataStoreService.GetSuplementForDeduction(eventType);
        }

        public async Task<bool> UpdateEventSuplement(EventType eventType, decimal suplement)
        {
            return await _dataStoreService.UpdateEventSuplement(eventType, suplement);
        }
    }
}
