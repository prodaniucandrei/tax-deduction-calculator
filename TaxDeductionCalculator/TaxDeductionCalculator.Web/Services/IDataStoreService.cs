﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Services
{
    public interface IDataStoreService
    {
        Task<decimal> GetCurrentTaxRate();
        Task<bool> UpdateCurrentTaxRate(decimal newTaxRate);
        Task<decimal> GetSuplementForDeduction(EventType eventType);
        Task<IList<EventInfo>> GetEventsWithInfo();
        Task<bool> UpdateEventSuplement(EventType eventType, decimal suplement);
    }
}