﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Services
{
    ///TODO Link this to any storage provider
    public class ComplexDataStoreService : IDataStoreService
    {
        public Task<decimal> GetCurrentTaxRate()
        {
            throw new NotImplementedException();
        }

        public Task<IList<EventInfo>> GetEventsWithInfo()
        {
            throw new NotImplementedException();
        }

        public Task<decimal> GetSuplementForDeduction(EventType eventType)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateCurrentTaxRate(decimal newTaxRate)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateEventSuplement(EventType eventType, decimal suplement)
        {
            throw new NotImplementedException();
        }
    }
}
