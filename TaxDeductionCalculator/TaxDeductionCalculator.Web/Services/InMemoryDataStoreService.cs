﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Services
{
    public class InMemoryDataStoreService : IDataStoreService
    {
        private decimal _currentTaxRatePercentage;
        private ConcurrentDictionary<EventType, decimal> _suplements;
        private IList<EventInfo> _eventsWithInfo;

        public InMemoryDataStoreService()
        {
            _currentTaxRatePercentage = 20;
            _suplements = new ConcurrentDictionary<EventType, decimal>();
            _suplements.TryAdd(EventType.Other, 0m);
            _suplements.TryAdd(EventType.Sports, 5m);
            _suplements.TryAdd(EventType.Political, 3m);

            _eventsWithInfo = new List<EventInfo> {
                new EventInfo(EventType.Sports, _suplements[EventType.Sports], 300000m ),
                new EventInfo(EventType.Political,_suplements[EventType.Political], 100000m),
                new EventInfo(EventType.Other, _suplements[EventType.Other], 50000m )
            };
        }

        public async Task<decimal> GetCurrentTaxRate()
        {
            return await Task.FromResult(_currentTaxRatePercentage);
        }

        public async Task<IList<EventInfo>> GetEventsWithInfo()
        {
            return await Task.FromResult(_eventsWithInfo);
        }

        public async Task<decimal> GetSuplementForDeduction(EventType eventType)
        {
            return await Task.FromResult(_suplements[eventType]);
        }

        public async Task<bool> UpdateCurrentTaxRate(decimal newTaxRate)
        {
            _currentTaxRatePercentage = newTaxRate;
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateEventSuplement(EventType eventType, decimal suplement)
        {
            _suplements[eventType] = suplement;
            UpdateEventsList(eventType, suplement);
            return await Task.FromResult(true);
        }

        private void UpdateEventsList(EventType eventType, decimal suplement)
        {
            _eventsWithInfo = _eventsWithInfo
                .Select(e => e.EventType == eventType ? new EventInfo(eventType, suplement, e.DonationLimit) : e)
                .ToList();
        }
    }
}
