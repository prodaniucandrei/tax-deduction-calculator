﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Contracts;
using TaxDeductionCalculator.Recommendation;
using TaxDeductionCalculator.Web.Models;
using TaxDeductionCalculator.Web.Repositories;

namespace TaxDeductionCalculator.Web.Controllers
{
    [Authorize(Roles = "Donor")]
    public class DonorController : Controller
    {
        private readonly IDeductionCalculator _deductionCalculator;
        private readonly IRecommendation _recommendationBreakDown;
        private readonly IDataStoreRepository _dataStoreRepository;

        public DonorController(IDeductionCalculator deductionCalculator, IDataStoreRepository dataStoreRepository, IRecommendation recommendationBreakDown)
        {
            _deductionCalculator = deductionCalculator;
            _dataStoreRepository = dataStoreRepository;
            _recommendationBreakDown = recommendationBreakDown;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var currentTaxRate = await _dataStoreRepository.GetCurrentTaxRate();
            ViewData["CurrentTaxRate"] = currentTaxRate;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ComputeDeduction(EventViewModel eventViewModel)
        {
            var taxRate = await _dataStoreRepository.GetCurrentTaxRate();
            var suplement = await _dataStoreRepository.GetSuplementForDeduction(eventViewModel.EventType);
            var deduction = _deductionCalculator.GetDeductibleAmountWithPrecision(eventViewModel.Amount, taxRate, suplement);

            ViewData["CurrentTaxRate"] = taxRate;
            ViewData["Deduction"] = deduction;
            return View("Index");
        }

        [HttpPost]
        public async Task<IActionResult> GetRecommendation(AmountViewModel amountViewModel)
        {
            var eventsInfo = await _dataStoreRepository.GetEventsWithInfo();
            var recommendations = _recommendationBreakDown.BreakDownAmount(amountViewModel.DonatedAmount, eventsInfo);
            ViewData["Recommendations"] = recommendations;
            return View("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ComputeDeductedAmount(EventViewModel eventViewModel)
        {
            var taxRate = await _dataStoreRepository.GetCurrentTaxRate();
            var suplement = await _dataStoreRepository.GetSuplementForDeduction(eventViewModel.EventType);
            var deduction = _deductionCalculator.GetDeductibleAmountWithPrecision(eventViewModel.Amount, taxRate, suplement);

            return Ok(deduction);
        }

        [HttpPost]
        public async Task<ActionResult<IList<EventRecommendationBreakDown>>> GetBreakDownRecommendation(AmountViewModel amountViewModel)
        {
            var eventsInfo = await _dataStoreRepository.GetEventsWithInfo();
            var recommendations = _recommendationBreakDown.BreakDownAmount(amountViewModel.DonatedAmount, eventsInfo);
            return Ok(recommendations);
        }
    }
}
