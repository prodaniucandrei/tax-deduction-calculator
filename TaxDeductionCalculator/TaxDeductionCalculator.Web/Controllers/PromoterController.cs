﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Web.Models;
using TaxDeductionCalculator.Web.Repositories;

namespace TaxDeductionCalculator.Web.Controllers
{
    [Authorize(Roles = "Promoter")]
    public class PromoterController : Controller
    {
        private readonly IDataStoreRepository _dataStoreRepository;

        public PromoterController(IDataStoreRepository dataStoreRepository)
        {
            _dataStoreRepository = dataStoreRepository;
        }

        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewData["Suplements"] = await _dataStoreRepository.GetEventsWithInfo();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateSuplement(SuplementViewModel suplementViewModel)
        {
            await _dataStoreRepository.UpdateEventSuplement(suplementViewModel.EventType, suplementViewModel.Suplement);
            ViewData["Suplements"] = await _dataStoreRepository.GetEventsWithInfo();
            return View("Index");
        }
    }
}
