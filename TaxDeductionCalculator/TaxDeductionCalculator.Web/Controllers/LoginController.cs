﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Web.Models;

namespace TaxDeductionCalculator.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        public LoginController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(AppUser appUser)
        {

            var user = await _userManager.FindByNameAsync(appUser.UserName);

            if (user != null)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(user, appUser.Password, false, false);

                if (signInResult.Succeeded)
                {
                    if (await _userManager.IsInRoleAsync(user, "Donor"))
                        return RedirectToAction("Index", "Donor");
                    else if (await _userManager.IsInRoleAsync(user, "Administrator"))
                        return RedirectToAction("Index", "Admin");
                    else if (await _userManager.IsInRoleAsync(user, "Promoter"))
                        return RedirectToAction("Index", "Promoter");
                    else 
                        return RedirectToAction("Index", "Login");

                }
            }

            return RedirectToAction("Index", "Login");
        }

    }
}
