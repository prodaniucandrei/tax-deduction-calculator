﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxDeductionCalculator.Web.Models;
using TaxDeductionCalculator.Web.Repositories;

namespace TaxDeductionCalculator.Web.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {

        private readonly IDataStoreRepository _dataStoreRepository;
        public AdminController(IDataStoreRepository dataStoreRepository)
        {
            _dataStoreRepository = dataStoreRepository;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var currentTaxRate = await _dataStoreRepository.GetCurrentTaxRate();
            ViewData["CurrentTaxRate"] = currentTaxRate;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTaxRate(TaxRateViewModel taxRateViewModel)
        {
            var updateResult = await _dataStoreRepository.UpdateCurrentTaxRate(taxRateViewModel.TaxRate);
            ViewData["CurrentTaxRate"] = taxRateViewModel.TaxRate;
            return View("Index");
        }

        [HttpGet]
        public IActionResult GetCurrentTaxRate()
        {
            var currentTaxRate = _dataStoreRepository.GetCurrentTaxRate();
            return Ok(currentTaxRate);
        }
        [HttpPost]
        public async Task<ActionResult<decimal>> UpdateCurrentTaxRate(TaxRateViewModel taxRateViewModel)
        {
            var updateResult = await _dataStoreRepository.UpdateCurrentTaxRate(taxRateViewModel.TaxRate);
            if (updateResult)
                return Ok(updateResult);
            else
                return StatusCode(500, "Update failed");
        }
    }
}
