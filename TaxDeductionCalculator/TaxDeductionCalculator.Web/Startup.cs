using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaxDeductionCalculator.Contracts;
using TaxDeductionCalculator.Recommendation;
using TaxDeductionCalculator.Web.Models;
using TaxDeductionCalculator.Web.Repositories;
using TaxDeductionCalculator.Web.Services;

namespace TaxDeductionCalculator.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var useCheaperDataStoreProvider = Configuration.GetValue<bool>("CheaperDataStoreProvider");
            if (useCheaperDataStoreProvider)
                services.AddSingleton<IDataStoreService, ComplexDataStoreService>();
            else
                services.AddSingleton<IDataStoreService, InMemoryDataStoreService>();

            services.AddDbContext<Data.DbContext>(config =>
            {
                config.UseInMemoryDatabase("MemoDb");
            });

            services.AddIdentity<AppUser, IdentityRole>(config =>
            {
                config.Password.RequireDigit = false;
                config.Password.RequiredLength = 2;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<Data.DbContext>();

            services.AddControllersWithViews();

            services.AddSingleton<IDeductionCalculator, DeductionCalculator>();
            services.AddSingleton<IDataStoreRepository, DataStoreRepository>();
            services.AddSingleton<IRecommendation, RecommendationBreakDown>();
            services.AddSingleton<IDeductionCalculator, DeductionCalculator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<AppUser> _userManager, RoleManager<IdentityRole> _roleManager)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Index}/{id?}");

            });
            RegisterMockUsers(_userManager, _roleManager);
        }

        private void RegisterMockUsers(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            roleManager.CreateAsync(new IdentityRole("Administrator")).Wait();
            roleManager.CreateAsync(new IdentityRole("Donor")).Wait();
            roleManager.CreateAsync(new IdentityRole("Promoter")).Wait();

            var donor = new AppUser
            {
                Id = "1",
                UserName = "donor",
                Password = "donor"
            };

            var admin = new AppUser
            {
                Id = "2",
                UserName = "admin",
                Password = "admin"
            };

            var promoter= new AppUser
            {
                Id = "3",
                UserName = "promoter",
                Password = "promoter"
            };

            var donorResult = userManager.CreateAsync(donor, donor.Password).Result;
            var addDonorRoleResult = userManager.AddToRoleAsync(donor, "Donor").Result;

            var adminResult = userManager.CreateAsync(admin, admin.Password).Result;
            var addAdminRoleResult = userManager.AddToRoleAsync(admin, "Administrator").Result;

            var promoterResult = userManager.CreateAsync(promoter, promoter.Password).Result;
            var addPromoterRoleResult = userManager.AddToRoleAsync(promoter, "Promoter").Result;
        }
    }
}
