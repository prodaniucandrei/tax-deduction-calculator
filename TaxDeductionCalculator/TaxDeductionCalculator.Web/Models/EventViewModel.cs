﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Models
{
    public class EventViewModel
    {
        [Range(0, 100.00)]
        public decimal Amount { get; set; }
        public EventType EventType { get; set; }
    }
}
