﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaxDeductionCalculator.Web.Models
{
    public class AmountViewModel
    {
        [Range(0, 100.00)]
        public decimal DonatedAmount { get; set; }
    }
}
