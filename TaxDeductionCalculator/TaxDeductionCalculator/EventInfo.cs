﻿namespace TaxDeductionCalculator
{
    public class EventInfo
    {
        private EventType _eventType;
        private decimal _suplement;
        private decimal _donationLimit;

        public EventInfo(EventType eventType, decimal suplement, decimal donationLimit)
        {
            _eventType = eventType;
            _suplement = suplement;
            _donationLimit = donationLimit;
        }

        public EventType EventType { get => _eventType; set { _eventType = value; } }
        public decimal DonationLimit { get => _donationLimit; set { _donationLimit = value; } }
        public decimal Supplement { get => _suplement; set { _suplement = value; } }
    }
}