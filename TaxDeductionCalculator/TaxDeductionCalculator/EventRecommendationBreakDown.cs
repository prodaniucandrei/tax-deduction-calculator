﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxDeductionCalculator
{
    public class EventRecommendationBreakDown
    {
        public EventType EventType { get; set; }
        public decimal Amount { get; set; }
        public EventRecommendationBreakDown(EventType eventType, decimal amount)
        {
            EventType = eventType;
            Amount = amount;
        }
    }
}
