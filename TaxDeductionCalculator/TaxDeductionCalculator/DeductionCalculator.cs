﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxDeductionCalculator.Contracts;

namespace TaxDeductionCalculator
{
    public class DeductionCalculator : IDeductionCalculator
    {
        public decimal GetDeductibleAmount(decimal amount, decimal taxRatePercentage, decimal suplement)
        {
            if (amount < 0 || taxRatePercentage < 0 || suplement < 0)
                throw new ArgumentOutOfRangeException("Input values must be positive.");
            return amount * (1 + suplement / 100) * ((taxRatePercentage / 100) / (100 - (taxRatePercentage / 100)));
        }

        public decimal GetDeductibleAmountWithPrecision(
            decimal amount,
            decimal taxRatePercentage,
            decimal suplement = 0,
            int precision = 2)
        {
            return
                decimal.Round(
                    this.GetDeductibleAmount(amount, taxRatePercentage, suplement),
                    precision,
                    MidpointRounding.ToEven
                    );
        }
    }
}
