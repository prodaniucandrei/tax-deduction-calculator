﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxDeductionCalculator.Contracts
{
    public interface IDeductionCalculator
    {
        decimal GetDeductibleAmount(decimal amount, decimal taxRatePercentage, decimal suplement);
        decimal GetDeductibleAmountWithPrecision(decimal amount, decimal taxRatePercentage, decimal suplement = 1, int precision = 2);
    }
}
